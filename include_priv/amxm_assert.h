#if !defined(__AMXM_ASSERT_H__)
#define __AMXM_ASSERT_H__

#include <string.h>

#ifdef __cplusplus
extern "C"
{
#endif

#ifdef AMXM_ASSERT_DEBUG

#include <stdio.h>

#define DPRINT printf("%s: %d\n", __FILE__, __LINE__)

#define when_null(x, l) if(x == NULL) { DPRINT; goto l; }
#define when_not_null(x, l) if(x != NULL) { DPRINT; goto l; }
#define when_true(x, l) if(x) { DPRINT; goto l; }
#define when_failed(x, l) if(x != 0) { DPRINT; goto l; }
#define when_str_empty(x, l) if(x == NULL || x[0] == '\0') { DPRINT; goto l; }
#define when_str_too_big(x, s, l) if(strlen(x) > s) { DPRINT; goto l; }


#else

#define when_null(x, l) if(x == NULL) { goto l; }
#define when_not_null(x, l) if(x != NULL) { goto l; }
#define when_true(x, l) if(x) { goto l; }
#define when_failed(x, l) if(x != 0) { goto l; }
#define when_str_empty(x, l) if(x == NULL || x[0] == '\0') { goto l; }
#define when_str_too_big(x, s, l) if(strlen(x) > s) { goto l; }

#endif


#ifdef __cplusplus
}
#endif

#endif // __AMXM_ASSERT_H__
