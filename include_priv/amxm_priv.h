#if !defined(__AMXM_PRIV_H__)
#define __AMXM_PRIV_H__

#ifdef __cplusplus
extern "C"
{
#endif

// doxygen has problems with these attributes
#if !defined(USE_DOXYGEN)
#define AMXM_PRIVATE __attribute__ ((visibility("hidden")))
#define AMXM_UNUSED __attribute__((unused))
#define AMXM_WARN_UNUSED_RETURN __attribute__ ((warn_unused_result))
#else
#define AMXM_PRIVATE
#define AMXM_CONSTRUCTOR
#define AMXM_DESTRUCTOR
#define AMXM_UNUSED
#endif

int AMXM_PRIVATE amxm_add_so(amxm_shared_object_t * const so);
void AMXM_PRIVATE amxm_so_remove_all_mods(amxm_shared_object_t *so);
bool AMXM_PRIVATE amxm_contains_so(const amxm_shared_object_t * const so);

#ifdef __cplusplus
}
#endif

#endif // __AMXM_PRIV_H__
