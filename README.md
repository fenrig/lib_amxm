# Modular C Implementation 

- [Introduction](#introduction)
- [Building](#building)
  - [Prerequisites](#prerequistes)
    - [Docker](#docker)
  - [Build amxp](#build-amxp)
- [Testing](#testing)
  - [Run the tests](#run-the-tests)
  - [Generate coverage reports](#generate-coverage-reports)
- [Install](#install)

## Introduction

This library makes it possible for an executable to load in shared objects (.so files), the shared object can then register functions that the executable can use. 

There are 2 available namespaces:
1) shared object namespace  (1 NS for 1 .so)
2) module namespace (can create on the fly)

## Building

### Prerequisites

#### Docker

Docker must be installed on your system.

If you have no clue how to do this here are some links that could help you:

 
- [Get Docker Engine - Community for Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-using-the-repository)
- [Get Docker Engine = Community for Debian](https://docs.docker.com/install/linux/docker-ce/debian/)
- [Get Docker Engine - Community for Fedora](https://docs.docker.com/install/linux/docker-ce/fedora/)
- [Get Docker Engine - Community for CentOS](https://docs.docker.com/install/linux/docker-ce/centos/)

### Build amxp

1. Clone the git repository

    ```bash
    mkdir -p ~/amx_project
    cd ~/amx_project
    git clone git@gitlab.com:proj_amx_01/libraries/lib_amxm.git
    ```

1. Fetch the container image

    ```bash
    docker pull registry.gitlab.com/proj_amx_01/ambiorix/amx_build:latest
    docker tag registry.gitlab.com/proj_amx_01/ambiorix/amx_build:latest amx_build:latest
    ```

1. Start a build container

    ```bash
    docker run -ti --rm -e "USER=$USER" -e "UID=$(id -u)" -e "GID=$(id -g)" -v ~/amx_project/:/home/$USER/amx_project amx_build:latest
    ```

    The `-v` option bind mounts the local directory in the container, at the exact same place.
    The `-e` options create environment variables in the container. These variables are used to create a user name with exactly the same user id and group id in the container as on your local host (User mapping).

1. Build it

    ```bash
    cd ~/amx_project/lib_amxp
    make
    ```

## Testing

### Run the tests

Use the same container as in the building step.
Make sure the container is still running. If not create a new one or restart the container.

```bash
cd ~/amx_project/lib_amxp
make test
```

### Coverage reports

The test target will generate coverage reports using [gcov](https://gcc.gnu.org/onlinedocs/gcc/Gcov.html) and [gcovr](https://gcovr.com/en/stable/guide.html)

A summary for each file ( *.c files) is printed in your console after the tests are run.
A HTML version of the coverage reports are also generate. These reports are available in the output directory of the compiler used.
Example: using native gcc
When the output of `gcc -dumpmachine` is `x86_64-linux-gnu`, the HTML coverage reports can be found at `~/amx_project/lib_amxp/output/x86_64-linux-gnu/coverage/report.`

## Install

### Using make target install

You can install your own compiled version easily in the container by running the install target.

```bash
cd ~/amx_project/lib_amxp
sudo make install
```

### Using package

From within the build container you can create packages.

```bash
cd ~/amx_project/lib_amxm
make package
```

The packages generated are:

- ~/amx_project/lib_amxm/libamxm-<VERSION>.tar.gz
- ~/amx_project/lib_amxm/libamxm-<VERSION>.deb

You can copy these packages and extract/install them.

For ubuntu or debian distributions use dpkg

```bash
sudo dpkg -i ~/amx_project/lib_amxm/libamxm-<VERSION>.deb
```

### Install from packagecloud

Pre-compiled debian (`.deb`) packages are available on package cloud.
You need to add the `ambiorix` packagecloud repository to your sources.

You can do this by downloading bash script and run it.

```bash
curl -s https://packagecloud.io/install/repositories/peter_deherdt/ambiorix/script.deb.sh | sudo bash
```

You can inspect the script first if you like.

```bash
cd ~/amx_project/lib_amxm
curl -s https://packagecloud.io/install/repositories/peter_deherdt/ambiorix/script.deb.sh > add_ambiorix_repo.sh

# Use your favorite editor to inspect the script

# If OK, run the script
sudo bash add_ambiorix_repo.sh
```

After this it becomes easy:

```bash
sudo apt install libamxm
```
