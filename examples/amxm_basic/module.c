#include <stdio.h>
#include <amxc/amxc_variant.h>
#include <amxc/amxc_llist.h>
#include <amxc/amxc_htable.h>
#include <amxm/amxm.h>

#define MOD "mod_basictest"

static amxm_shared_object_t *so = NULL;
static amxm_module_t *mod = NULL;

int my_testing_function(const char const *function_name, amxc_var_t *args, amxc_var_t *ret) {
    printf("%s(): my_testing_function\n", function_name);
    return 0;
}

AMXM_CONSTRUCTOR module_init(void) {
    printf(">>> Module constructor\n");
    int ret = 0;

    so = amxm_mod_current_shared_object_get();
    if(so == NULL) {
        printf("Couldnt get shared object\n");
        return 1;
    }

    ret = amxm_module_register(&mod, so, MOD);
    if(ret != 0) {
        printf("Couldnt make module\n");
        return 1;
    }

    ret = amxm_module_add_function(mod, "test", my_testing_function);
    printf("int ret: %d\n", ret);


    printf("<<< Module constructor\n");
    return ret;
}

AMXM_DESTRUCTOR module_exit(void) {
    printf(">>> Module destructor\n");

    printf("<<< Module destructor\n");
    return 0;
}
