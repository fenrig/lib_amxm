#include <stdio.h>
#include <stdlib.h>
#include <amxc/amxc_variant.h>
#include <amxc/amxc_llist.h>
#include <amxc/amxc_htable.h>
#include <amxm/amxm.h>

int main(void) {
    int error = 0;
    amxm_shared_object_t *so = NULL;
    amxc_var_t *args = NULL;
    amxc_var_t *ret = NULL;

    int module_ret = amxm_so_open(&so, "module", "./module.so");
    if(module_ret) {
        printf("Cannot load module\n");
        return -1;
    }

    amxc_var_new(&args);
    amxc_var_new(&ret);

    printf("exec: execute function with opened module\n");
    printf(NULL);

    error = amxm_execute_function("module",
                                  "mod_basictest",
                                  "test",
                                  args,
                                  ret);
    printf("Execute with module opened: int Error: %d\n", error);
    if(error != 0) {
        fprintf(stderr, "Executing function with module open: FAILED", 30);
        return -1;
    }

    // Get first (and only module name)
    char *first_module_name = amxm_module_probe(so, 0);
    printf("First module name: %s\n", first_module_name);
    free(first_module_name);

    module_ret = amxm_so_close(&so);
    if(module_ret) {
        printf("Cannot unload module\n");
        return -1;
    }

    error = amxm_execute_function("module",
                                  "mod_basictest",
                                  "test",
                                  args,
                                  ret);
    printf("Execute with module closed: int Error: %d\n", error);
    if(error == 0) {
        fprintf(stderr, "Executing function with module closed: FAILED", 30);
        return -1;
    }

    first_module_name = amxm_module_probe(so, 0);
    printf("First module name: %p\n", first_module_name);

    amxc_var_delete(&args);
    amxc_var_delete(&ret);

    return 0;
}
