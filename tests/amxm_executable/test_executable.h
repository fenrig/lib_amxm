/****************************************************************************
**
** - DISCLAIMER OF WARRANTY -
**
** THIS FILE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER
** EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
** PURPOSE.
**
** THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE SOURCE
** CODE IS WITH YOU. SHOULD THE SOURCE CODE PROVE DEFECTIVE, YOU
** ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
**
** - LIMITATION OF LIABILITY -
**
** IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN
** WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES
** AND/OR DISTRIBUTES THE SOURCE CODE, BE LIABLE TO YOU FOR DAMAGES,
** INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
** ARISING OUT OF THE USE OR INABILITY TO USE THE SOURCE CODE
** (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED
** INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE
** OF THE SOURCE CODE TO OPERATE WITH ANY OTHER PROGRAM), EVEN IF SUCH
** HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
** DAMAGES.
**
****************************************************************************/

#ifndef __TEST_EXECUTABLE_H__
#define __TEST_EXECUTABLE_H__

#include <amxc/amxc_variant.h>

void rand_str(char *charset, size_t charset_len, char *dest, size_t dest_len);
void gen_module_namespace_str(char *dest, size_t dest_len);
void gen_function_name_str(char *dest, size_t dest_len);

void test_open_shared_object_args(void **state);
void test_close_shared_object_args(void **state);
void test_get_shared_object_args(void **satet);
void test_get_module_args(void **state);
void test_open_close_shared_object(void **state);
void test_open_close_shared_object_by_name(void **state);

void test_register_module_args(void **state);
void test_close_module_args(void **state);
void test_get_number_of_modules_args(void **state);
void test_probe_modules(void **state);
void test_get_module(void **state);
void test_modules(void **state);

int my_testing_function(const char * const function_name, amxc_var_t *args, amxc_var_t *ret);
int my_other_testing_function(const char * const function_name, amxc_var_t *args, amxc_var_t *ret);
void test_has_function(void **state);
void test_register_function_args(void **state);
void test_execute_function_args(void **state);
void test_delete_function_args(void **state);
void test_functions(void **state);
void stress_test_functions(void **state);

#endif // __TEST_EXECUTABLE_H__
