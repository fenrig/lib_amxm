#include <stdio.h>

#include <amxc/amxc_variant.h>
#include <amxc/amxc_llist.h>
#include <amxc/amxc_htable.h>

#include <amxm/amxm.h>

#include <amxm_priv.h>

#include "test_module.h"

#define MOD "mod_basictest"

AMXM_CONSTRUCTOR module_init(void) {
    return 0;
}

AMXM_DESTRUCTOR module_exit(void) {
    return 0;
}
