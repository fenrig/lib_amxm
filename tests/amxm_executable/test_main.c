/****************************************************************************
**
** - DISCLAIMER OF WARRANTY -
**
** THIS FILE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER
** EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
** PURPOSE.
**
** THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE SOURCE
** CODE IS WITH YOU. SHOULD THE SOURCE CODE PROVE DEFECTIVE, YOU
** ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
**
** - LIMITATION OF LIABILITY -
**
** IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN
** WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES
** AND/OR DISTRIBUTES THE SOURCE CODE, BE LIABLE TO YOU FOR DAMAGES,
** INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
** ARISING OUT OF THE USE OR INABILITY TO USE THE SOURCE CODE
** (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED
** INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE
** OF THE SOURCE CODE TO OPERATE WITH ANY OTHER PROGRAM), EVEN IF SUCH
** HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
** DAMAGES.
**
****************************************************************************/

#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "test_executable.h"

int main(void) {
    const struct CMUnitTest tests[] = {
        // shared object
        cmocka_unit_test(test_open_shared_object_args),
        cmocka_unit_test(test_close_shared_object_args),
        cmocka_unit_test(test_get_shared_object_args),
        cmocka_unit_test(test_open_close_shared_object),
        cmocka_unit_test(test_open_close_shared_object_by_name),
        // module
        cmocka_unit_test(test_get_module_args),
        cmocka_unit_test(test_register_module_args),
        cmocka_unit_test(test_close_module_args),
        cmocka_unit_test(test_get_number_of_modules_args),
        cmocka_unit_test(test_probe_modules),
        cmocka_unit_test(test_get_module),
        cmocka_unit_test(test_modules),
        // functions
        cmocka_unit_test(test_has_function),
        cmocka_unit_test(test_register_function_args),
        cmocka_unit_test(test_execute_function_args),
        cmocka_unit_test(test_delete_function_args),
        cmocka_unit_test(test_functions),
        cmocka_unit_test(stress_test_functions)
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}
