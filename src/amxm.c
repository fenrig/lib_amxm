#include <stdlib.h>
#include <stddef.h>
#include <stdlib.h>
#include <dlfcn.h>

#include <amxc/amxc_variant.h>
#include <amxc/amxc_llist.h>
#include <amxc/amxc_htable.h>
#include <amxc/amxc_hash.h>

#include <amxm/amxm.h>

#include <amxm_assert.h>
#include <amxm_priv.h>

/**
   @file
   @brief
   Ambiorix module API implementation
 */

static amxc_llist_t amxm_so_llist;

int amxm_add_so(amxm_shared_object_t * const so) {
    return amxc_llist_append(&amxm_so_llist, &so->it);
}

bool amxm_contains_so(const amxm_shared_object_t * const so) {
    amxm_shared_object_t *reg_so = NULL;

    amxc_llist_for_each(it, (&amxm_so_llist)) {
        reg_so = amxc_llist_it_get_data(it, amxm_shared_object_t, it);
        if(reg_so == so) {
            break;
        }
        reg_so = NULL;
    }

    return (reg_so != NULL);
}

int amxm_close_so(const char * const shared_object_name) {
    int retval = -2;
    amxm_shared_object_t *so = NULL;

    when_str_empty(shared_object_name, exit);
    when_str_too_big(shared_object_name, AMXM_SHARED_OBJECT_LENGTH, exit);

    so = amxm_get_so(shared_object_name);
    when_null(so, exit);

    retval = amxm_so_close(&so);

exit:
    return retval;
}

amxm_shared_object_t *amxm_get_so(const char * const shared_object_name) {
    amxm_shared_object_t *ret = NULL;
    const char *so_name = shared_object_name;

    if((so_name == NULL) || (*so_name == 0)) {
        so_name = "self";
    }

    amxc_llist_for_each(it, (&amxm_so_llist)) {
        ret = amxc_llist_it_get_data(it, amxm_shared_object_t, it);
        if(strncmp(ret->name, so_name, AMXM_MODULE_NAME_LENGTH) == 0) {
            break;
        }
        ret = NULL;
    }

    return ret;
}

amxm_module_t *amxm_get_module(const char * const shared_object_name,
                               const char * const module_name) {

    amxm_module_t *ret = NULL;
    // No verification of arguments are done here,
    // the called functions will do the verification anyway
    // see amxm_get_so and amxm_so_get_module
    amxm_shared_object_t *so = amxm_get_so(shared_object_name);
    when_null(so, exit);

    ret = amxm_so_get_module(so, module_name);

exit:
    return ret;
}

int amxm_execute_function(const char * const shared_object_name,
                          const char * const module_name,
                          const char * const func_name,
                          amxc_var_t *args,
                          amxc_var_t *ret) {
    int error = -1;
    // No verification of arguments are done here,
    // the called functions will do the verification anyway
    // see amxm_get_so and amxm_so_execute_function
    amxm_shared_object_t *so = amxm_get_so(shared_object_name);
    when_null(so, exit);

    error = amxm_so_execute_function(so, module_name, func_name, args, ret);

exit:
    return error;
}

bool amxm_has_function(const char * const shared_object_name,
                       const char * const module_name,
                       const char * const func_name) {
    bool retval = false;
    amxm_shared_object_t *so = NULL;
    // No verification of arguments are done here,
    // the called functions will do the verification anyway
    // see amxm_get_so and amxm_so_has_function

    so = amxm_get_so(shared_object_name);
    when_null(so, exit);

    retval = amxm_so_has_function(so, module_name, func_name);

exit:
    return retval;
}

static AMXM_CONSTRUCTOR amxm_register_self(void) {
    amxm_shared_object_t *so = (amxm_shared_object_t *) calloc(1, sizeof(amxm_shared_object_t));
    when_null(so, exit_free);

    when_failed(amxc_llist_init(&so->amxm_module_llist), exit_free);

    strncpy(so->name, "self", AMXM_MODULE_NAME_LENGTH);

    when_failed(amxc_llist_append(&amxm_so_llist, &so->it), exit_free);

    return 0;

exit_free:
    free(so);
    return -1;
}

static AMXM_DESTRUCTOR amxm_deregister_self(void) {
    amxm_shared_object_t *so = amxm_get_so("self");

    amxc_llist_it_take(&(so->it));
    amxm_so_remove_all_mods(so);

    free(so);
    return 0;
}