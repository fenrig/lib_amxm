#include <stdlib.h>
#include <dlfcn.h>

#include <amxc/amxc_variant.h>
#include <amxc/amxc_htable.h>
#include <amxc/amxc_llist.h>
#include <amxm/amxm.h>

#include <amxm_assert.h>
#include <amxm_priv.h>

static amxm_shared_object_t *current_shared_object;

void amxm_so_remove_all_mods(amxm_shared_object_t *so) {
    for(amxc_llist_it_t *it = amxc_llist_get_first(&so->amxm_module_llist); it;) {
        amxm_module_t *mod = amxc_llist_it_get_data(it, amxm_module_t, it);
        it = amxc_llist_it_get_next(it);
        amxm_module_deregister(&mod);
    }
}

amxm_shared_object_t *amxm_so_get_current(void) {
    return current_shared_object;
}

int amxm_so_open(amxm_shared_object_t **so,
                 const char *shared_object_name,
                 const char * const path_to_so) {
    void *handle = NULL;

    when_null(so, exit);
    when_str_empty(shared_object_name, exit);
    when_str_empty(path_to_so, exit);
    when_str_too_big(shared_object_name, AMXM_SHARED_OBJECT_LENGTH, exit);

    when_not_null(amxm_get_so(shared_object_name), exit);

    *so = (amxm_shared_object_t *) calloc(1, sizeof(amxm_shared_object_t));
    when_null(so, exit);

    current_shared_object = *so;

    when_failed(amxc_llist_init(&(*so)->amxm_module_llist), exit_free);

    strncpy((*so)->name, shared_object_name, AMXM_MODULE_NAME_LENGTH);

    handle = dlopen(path_to_so, RTLD_LAZY);
    current_shared_object = NULL;
    when_null(handle, exit_free);
    (*so)->handle = handle;

    when_failed(amxm_add_so(*so), exit_dl);

    return 0;

exit_dl:
    dlclose((*so)->handle);
exit_free:
    free(*so);
exit:
    *so = NULL;
    return -1;
}

int amxm_so_close(amxm_shared_object_t **so) {
    int retval = -1;
    int ret = 0;

    when_null(so, exit);

    when_true(!amxm_contains_so(*so), exit);

    current_shared_object = *so;
    ret = dlclose((*so)->handle);
    current_shared_object = NULL;
    when_failed(ret, exit);

    amxc_llist_it_take(&(*so)->it);
    amxm_so_remove_all_mods(*so);

    free(*so);
    *so = NULL;
    retval = 0;

exit:
    return retval;
}

size_t amxm_so_count_modules(const amxm_shared_object_t * const shared_object) {
    size_t retval = 0;
    when_null(shared_object, exit);

    retval = amxc_llist_size(&(shared_object->amxm_module_llist));

exit:
    return retval;
}

char *amxm_so_probe(const amxm_shared_object_t * const shared_object, size_t index) {
    amxm_module_t *mod = NULL;
    char *ret = NULL;
    size_t count_of_modules = 0;
    amxc_llist_it_t *it = NULL;

    when_null(shared_object, exit);

    count_of_modules = amxm_so_count_modules(shared_object);
    when_true(index >= count_of_modules, exit);

    it = amxc_llist_get_at(&shared_object->amxm_module_llist, index);

    mod = amxc_llist_it_get_data(it, amxm_module_t, it);
    when_null(mod, exit);

    ret = (char *) calloc(1, AMXM_MODULE_NAME_LENGTH + 1);
    when_null(ret, exit);

    memcpy(ret, mod->name, AMXM_MODULE_NAME_LENGTH);

exit:
    return ret;
}

amxm_module_t *amxm_so_get_module(const amxm_shared_object_t * const shared_object,
                                  const char * const module_name) {
    amxm_module_t *ret = NULL;
    const amxm_shared_object_t *so = shared_object;

    when_null(module_name, exit);
    when_str_empty(module_name, exit);
    when_str_too_big(module_name, AMXM_MODULE_NAME_LENGTH, exit);

    if(shared_object == NULL) {
        so = amxm_get_so("self");
    }
    when_null(so, exit);

    amxc_llist_for_each(it, (&so->amxm_module_llist)) {
        ret = amxc_llist_it_get_data(it, amxm_module_t, it);
        if(strncmp(ret->name, module_name, AMXM_MODULE_NAME_LENGTH) == 0) {
            break;
        }
        ret = NULL;
    }

exit:
    return ret;
}

int amxm_so_remove_function(amxm_shared_object_t * const so,
                            const char * const module_name,
                            const char * const func_name) {
    int retval = -3;
    amxm_module_t *mod = NULL;

    when_null(so, exit);
    when_null(module_name, exit);
    when_str_empty(module_name, exit);

    mod = amxm_so_get_module(so, module_name);
    when_null(mod, exit);

    retval = amxm_module_remove_function(mod, func_name);

exit:
    return retval;
}

int amxm_so_execute_function(amxm_shared_object_t * const so,
                             const char * const module_name,
                             const char * const func_name,
                             amxc_var_t *args,
                             amxc_var_t *ret) {
    int retval = -1;
    amxm_module_t *mod = NULL;

    when_null(so, exit);

    mod = amxm_so_get_module(so, module_name);
    when_null(mod, exit);

    retval = amxm_module_execute_function(mod, func_name, args, ret);

exit:
    return retval;
}

bool amxm_so_has_function(const amxm_shared_object_t * const so,
                          const char * const module_name,
                          const char * const func_name) {

    bool retval = false;
    // No verification of namearguments are done here,
    // the called functions will do the verification anyway
    // see amxm_so_get_module and amxm_module_has_function
    amxm_module_t *mod = NULL;
    when_null(so, exit);

    mod = amxm_so_get_module(so, module_name);
    when_null(mod, exit);

    retval = amxm_module_has_function(mod, func_name);

exit:
    return retval;
}