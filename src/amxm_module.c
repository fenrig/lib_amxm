#include <stdlib.h>
#include <dlfcn.h>

#include <amxc/amxc_variant.h>
#include <amxc/amxc_hash.h>
#include <amxc/amxc_htable.h>
#include <amxc/amxc_llist.h>

#include <amxm/amxm.h>

#include <amxm_assert.h>
#include <amxm_priv.h>

static void function_cb_htable_it_free(AMXM_UNUSED const char *key, amxc_htable_it_t *it) {
    amxm_function_callback_t *cb = NULL;

    cb = amxc_htable_it_get_data(it, amxm_function_callback_t, it);
    free(cb);
}

static amxm_function_callback_t *amxm_function_callback_get(const amxc_htable_t * const func_htable,
                                                            const char * const function_name) {
    const char *key = NULL;
    amxm_function_callback_t *ret = NULL;
    amxc_htable_it_t *it = NULL;

    it = amxc_htable_get(func_htable, function_name);
    when_null(it, exit);

    key = amxc_htable_it_get_key(it);
    when_str_empty(key, exit);

    ret = amxc_htable_it_get_data(it, amxm_function_callback_t, it);

exit:
    return ret;
}

static inline amxc_htable_t *amxm_function_htable_get(amxm_module_t *mod) {
    return &mod->amxm_function_htable;
}


int amxm_module_register(amxm_module_t **mod,
                         amxm_shared_object_t * const shared_object,
                         const char * const module_name) {
    int err = -1;
    amxm_shared_object_t *so = shared_object;

    when_null(mod, exit);
    when_str_empty(module_name, exit);
    when_str_too_big(module_name, AMXM_MODULE_NAME_LENGTH, exit);

    if(so == NULL) {
        so = amxm_get_so("self");
    }
    when_null(so, exit);

    when_not_null(amxm_so_get_module(so, module_name), exit);

    *mod = (amxm_module_t *) calloc(1, sizeof(amxm_module_t));
    when_null(*mod, exit);
    strncpy((*mod)->name, module_name, AMXM_MODULE_NAME_LENGTH);

    err = amxc_llist_append(&so->amxm_module_llist, &(*mod)->it);
    when_failed(err, exit_free);

    err = amxc_htable_init(&(*mod)->amxm_function_htable, 0);
    when_failed(err, exit);
    amxc_htable_set_hash_func(&(*mod)->amxm_function_htable, amxc_DJB_hash);

    return 0;

exit_free:
    free(*mod);
exit:
    if(mod != NULL) {
        *mod = NULL;
    }
    return err;
}

int amxm_module_deregister(amxm_module_t **mod) {
    int retval = -1;
    when_null(mod, exit);
    when_null(*mod, exit);

    amxc_llist_it_take(&(*mod)->it);
    amxc_htable_clean(&(*mod)->amxm_function_htable, function_cb_htable_it_free);
    free(*mod);
    *mod = NULL;

    retval = 0;

exit:
    return retval;
}

int amxm_module_add_function(amxm_module_t * const mod,
                             const char * const func_name,
                             amxm_callback_t cb) {
    amxm_function_callback_t *func_cb = NULL;

    when_str_empty(func_name, exit);
    when_str_too_big(func_name, AMXM_FUNCTION_NAME_LENGTH, exit);

    when_null(mod, exit);

    // check if function already exists in htable
    func_cb = amxm_function_callback_get(&mod->amxm_function_htable, func_name);
    if(func_cb) {
        // yes --> overwrite callback pointer
        // overwriting with NULL is possible
        func_cb->function_cb = cb;
    } else {
        // no --> new function cb
        when_null(cb, exit);
        // create structure that goes into htable
        func_cb = (amxm_function_callback_t *) calloc(1, sizeof(amxm_function_callback_t));
        when_null(func_cb, exit);

        func_cb->function_cb = cb;
        strncpy(func_cb->function_name, func_name, AMXM_FUNCTION_NAME_LENGTH);

        when_failed(amxc_htable_insert(&mod->amxm_function_htable,
                                       func_cb->function_name,
                                       &func_cb->it), exitfree);
    }

    return 0;

exitfree:
    free(func_cb);
exit:
    return -1;
}

int amxm_module_remove_function(amxm_module_t * const mod,
                                const char * const func_name) {
    int retval = -2;
    amxc_htable_t *ht = NULL;
    amxm_function_callback_t *cb = NULL;

    when_null(mod, exit);
    when_null(func_name, exit);
    when_str_empty(func_name, exit);
    when_str_too_big(func_name, AMXM_FUNCTION_NAME_LENGTH, exit);

    ht = amxm_function_htable_get(mod);
    when_null(mod, exit);

    cb = amxm_function_callback_get(ht, func_name);
    when_null(cb, exit);

    amxc_htable_it_clean(&(cb->it), function_cb_htable_it_free);

    retval = 0;

exit:
    return retval;
}

int amxm_module_execute_function(amxm_module_t *mod,
                                 const char * const func_name,
                                 amxc_var_t *args,
                                 amxc_var_t *ret) {
    int error = -1;
    amxm_callback_t cb;
    amxc_htable_t *func_htable = NULL;
    amxm_function_callback_t *function_cb = NULL;

    when_str_empty(func_name, exit);
    when_str_too_big(func_name, AMXM_FUNCTION_NAME_LENGTH, exit);

    when_null(args, exit);
    when_null(ret, exit);
    when_null(mod, exit);

    func_htable = amxm_function_htable_get(mod);
    when_null(func_htable, exit);

    function_cb = amxm_function_callback_get(func_htable, func_name);
    when_null(function_cb, exit);

    cb = function_cb->function_cb;
    when_null(cb, exit);

    error = cb(func_name, args, ret);

exit:
    return error;
}

bool amxm_module_has_function(amxm_module_t *mod,
                              const char * const func_name) {
    bool retval = false;
    amxc_htable_t *func_htable = NULL;

    when_str_empty(func_name, exit);
    when_str_too_big(func_name, AMXM_FUNCTION_NAME_LENGTH, exit);
    when_null(mod, exit);

    func_htable = amxm_function_htable_get(mod);

    retval = amxc_htable_contains(func_htable, func_name);

exit:
    return retval;
}
